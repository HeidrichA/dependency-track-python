import dependency_track as dt


def main():
    dt.BOM("test", "0.0.0", "bom.json")
    dp = dt.Project("test", "0.0.0")
    dp.add_tag("bla")
    dp.classifier = dt.Classifier.container
    dp.commit()


if __name__ == "__main__":
    main()
