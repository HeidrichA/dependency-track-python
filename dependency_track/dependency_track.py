#!/bin/env python3

from dbm.ndbm import library
from enum import Enum
from platform import version
from unicodedata import name
import requests
from argparse import ArgumentParser
from os import device_encoding, environ
from sys import stdin
from urllib3 import encode_multipart_formdata


class Classifier():
    application = 'APPLICATION'
    framework = 'FRAMEWORK'
    library = 'LIBRARY'
    container = 'CONTAINER'
    os = 'OPERATING_SYSTEM'
    device = 'DEVICE'
    firmware = 'FIRMWARE'
    file = 'FILE'


class DependencyTrack():
    TAG_SEPARATOR = " "
    AUTO_COMMIT = False
    TRUST_ENV = False

    def __init__(self, base: str = None, key: str = None):
        self.api = f"{base}/api/v1" if base else f"{environ['DTRACK_BASE_URL']}/api/v1"
        self.key = key if key else environ["DTRACK_ADMIN_TOKEN"]
        self.session = requests.Session()
        self.session.trust_env = self.TRUST_ENV
        self.session.headers.update(
            {"X-Api-Key": f"{self.key}", "X-Total-Count": '5aa0'})

    def close(self):
        self.session.close()


class BOM(DependencyTrack):
    def __init__(self, name: str, version: str, bom: str = None, base: str = None, key: str = None, bom_data: str = None):
        super().__init__(base, key)
        if bom_data:
            assert bom is None, "No bom_data and bom_file can be specified together"
            self.bom_data = bom_data
        else:
            if bom is None:
                bom_file = stdin
            else:
                bom_file = open(bom, "r")
            self.bom_data = "".join(bom_file.readlines())

        body, header = encode_multipart_formdata({
            "projectName": name,
            "projectVersion": version,
            "autoCreate": "true",
            "bom": ("bom", self.bom_data)
        })
        self.session.headers.update({"Content-Type": header})
        result = self.session.post(f"{self.api}/bom", body)
        assert result.status_code == 200, f"error ({result.status_code}) uploading bom"


class Project(DependencyTrack):
    def __init__(self, name: str, version: str, base: str = None, key: str = None):
        super().__init__(base, key)
        assert name and version, "Project name and version are required"
        self.name = name
        self.version = version
        self.read()

    def read(self):
        self.lookup(self.name, self.version)

    def lookup(self, name, version):
        params = {"name": name, "version": version}
        result = self.session.get(f"{self.api}/project/lookup", params=params)
        assert result.status_code == 200, f"Error ({result.status_code}) looking up the project ({self.api}/project/lookup {params})"
        self.project_data = result.json()
        return self.project_data

    def update(self, project_data: dict):
        result = self.session.post(
            f"{self.api}/project", json=project_data)
        assert result.status_code == 200, f"error ({result.status_code}: '{result.reason}') when updating project with data: '{project_data}'"
        return(result.json())

    def commit(self):
        self.update(self.project_data)

    @property
    def classifier(self):
        return(self.project_data["classifier"]) if "classifier" in self.project_data else None

    @classifier.setter
    def classifier(self, cls):
        self.project_data["classifier"] = cls
        if self.AUTO_COMMIT:
            self.commit()

    def add_tag(self, tag: str):
        tags = self.tags
        self.tags = tags + [{"name": tag}]

    def remove_tag(self, the_tag):
        tags = self.tags
        self.tags = [t for t in tags if t["name"] != the_tag]

    @property
    def tags(self):
        return(self.project_data["tags"] if "tags" in self.project_data else [])

    @tags.setter
    def tags(self, tags):
        self.project_data["tags"] = tags
        if self.AUTO_COMMIT:
            self.commit()


def main():
    parser = ArgumentParser(
        description="Do a few basic stuff on dependency-track API")
    parser.add_argument("--base", required=False)
    parser.add_argument("--key", required=False)
    parser.add_argument("--project", required=True)
    parser.add_argument("--version", required=True)
    parser.add_argument("--bomfile", required=False)
    parser.add_argument("--classifier", required=False)
    parser.add_argument("--tags", required=False)
    parser.add_argument("--reset_tags", required=False,
                        type=bool, default=False)
    arguments = parser.parse_args()
    api = f"{arguments.base}/api/v1" if arguments.base else None
    key = arguments.key if arguments.key else None
    if arguments.bomfile:
        BOM(arguments.project, arguments.version,
            arguments.bomfile, base=api, key=key)
    dp = Project(arguments.project, arguments.version, api, key)
    commit_required = False
    if arguments.reset_tags:
        dp.tags = []
        commit_required = True
    if arguments.tags:
        for element in arguments.tags.split(dp.TAG_SEPARATOR):
            if element:
                dp.add_tag(element)
                commit_required = True
    if arguments.classifier:
        dp.classifier = arguments.classifier
        commit_required = True
    if commit_required:
        dp.commit()


if __name__ == "__main__":
    main()
